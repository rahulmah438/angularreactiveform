import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl} from '@angular/forms';
import {Validators} from '@angular/forms'
import { Router } from '@angular/router'
import { DataTransferService} from '../data-transfer.service';
import {Fieldname} from '../fieldname'



@Component({
  selector: 'app-form-fill',
  templateUrl: './form-fill.component.html',
  styleUrls: ['./form-fill.component.css']
})
export class FormFillComponent implements OnInit{
  UserDetails = new Fieldname();

  RegisterForm = new FormGroup({
    firstname: new FormControl('',[
     Validators.required,
     Validators.pattern("[a-zA-Z ]+")]),
     
    
    lastname: new FormControl('',[ 
    Validators.required,
    Validators.pattern("[a-zA-Z ]*")
    ]),

    gender: new FormControl('',[
     Validators.required,
      Validators.pattern("m|f|M|F"),
     
    ]),

    contactnum: new FormControl('',[
      Validators.required,
      Validators.maxLength(10),
      Validators.pattern("[0-9]+")
     
    ]),
    empid:new FormControl('',[
      Validators.required,
      Validators.pattern("[0-9]+"),
      Validators.maxLength(4),
    ]),
    password:new FormControl('',[
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(16),
      Validators.pattern('((?=.*[0-9])(?=.*[a-z])(?=.*[$@$!%*?&()_+={};;"|,.<>]).{0,16})')
    ]),
          
    confirmpassword:new FormControl('',[
    Validators.required,
    Validators.maxLength(16),
    Validators.pattern('((?=.*[0-9])(?=.*[a-z])(?=.*[$@$!%*?&()_+={};;"|,.<>]).{0,16})')
  ]),
      
  })
  
  
  
  constructor(private router : Router, private DTservice : DataTransferService){

  }
  ngOnInit() {
    if (window.location.pathname == "/formfill") {
        this.UserDetails = new Fieldname();
        this.UserDetails = this.DTservice.getData();
        this.RegisterForm.patchValue ({
          firstname : this.UserDetails.firstname,
          lastname : this.UserDetails.lastname,
          gender : this.UserDetails.gender,
          contactnum : this.UserDetails.contactnum,
          empid : this.UserDetails.empid,
          password : this.UserDetails.password,
          confirmpassword : this.UserDetails.confirmpassword
        })
    }
  }
  
  check : boolean = true;
  CheckPassword(){
    if(this.RegisterForm.value.password!=this.RegisterForm.value.confirmpassword)
    {
      console.log("Incorrect Password")
       this.check=false;
    }
    else{
      console.log("Correct Password")
      this.check=true;
    }
  }
  
  OnSubmit(){
    this.DTservice.setData(this.RegisterForm.value)
    this.router.navigate(['/formdetails'])
  }
EyeFlag : Boolean = false;
changeID (id) {
  if (this.EyeFlag) {
    document.getElementById(id).setAttribute('type', 'password');
  }
  else {
    document.getElementById(id).setAttribute('type', 'text');
  }
  this.EyeFlag = !this.EyeFlag;
}


}
