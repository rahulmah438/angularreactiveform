import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { DataTransferService} from '../data-transfer.service';

@Component({
  selector: 'app-form-details',
  templateUrl: './form-details.component.html',
  styleUrls: ['./form-details.component.css']
})
export class FormDetailsComponent implements OnInit {
  details: object;
  

  constructor(private router : Router, private DTservice : DataTransferService){

  }

  ngOnInit() {
    this.details = this.DTservice.getData();
    var table = document.getElementById("FormData");
    
  }
 
  
  EyeFlag : Boolean = false;
changeID (id) {
  if (this.EyeFlag) {
    document.getElementById(id).setAttribute('type', 'password');
  }
  else {
    document.getElementById(id).setAttribute('type', 'text');
  }
  this.EyeFlag = !this.EyeFlag;
}
  
  EditDetails(){
    this.router.navigate( ["/formfill"]);
  }

}
 
 
 
  

