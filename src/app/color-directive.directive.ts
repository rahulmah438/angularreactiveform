import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appColorDirective]'
})
export class ColorDirectiveDirective {

  constructor(private ref : ElementRef) {
    this.changeColor('black')
   }
   changeColor(color : any) {
     this.ref.nativeElement.style.color = color;
   }
}
