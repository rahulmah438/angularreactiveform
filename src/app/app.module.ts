import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms'
import { AppComponent } from './app.component';
import { FormGroup, FormControl } from '@angular/forms';
import { FormDetailsComponent } from './form-details/form-details.component';
import {RouterModule, Routes} from '@angular/router';
import { FormFillComponent } from './form-fill/form-fill.component';
import { ColorDirectiveDirective } from './color-directive.directive'; 
import { DataTransferService} from './data-transfer.service';

const routes : Routes = [{
  path : '',
  component : FormFillComponent
},
{  
  path : 'formfill',
  component : FormFillComponent
},
{
  path : 'formdetails',
  component : FormDetailsComponent
}]

@NgModule({
  declarations: [
    AppComponent,
    FormDetailsComponent,
    FormFillComponent,
    ColorDirectiveDirective

  ], 
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
  DataTransferService
],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
