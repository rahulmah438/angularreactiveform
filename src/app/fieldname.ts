export class Fieldname {

    public firstname: string;
    public lastname: string;
    public gender: string;
    public contactnum: string;
    public empid: string;
    public password: string;
    public confirmpassword: string;
}
